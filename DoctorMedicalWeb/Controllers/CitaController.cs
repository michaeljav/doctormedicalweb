﻿using DoctorMedicalWeb.App_Data;
using DoctorMedicalWeb.Libreria;
using DoctorMedicalWeb.Models;
using DoctorMedicalWeb.ModelsComplementarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoctorMedicalWeb.Controllers
{
    public class CitaController : Controller
    {
        //
        // GET: /Cita/
        string controler = "Cita", vista = "Ini_Cita", PaginaAutorizada = Paginas.pag_Cita.ToString();
        public ActionResult Ini_Cita()
        {
            //Si NO esta loguieado lo redireccionara al loguin
            if (HttpContext.Session["user"] == null)
            {
                return RedirectToAction("Index", "PaginaPresentacion");
            }

            //lleno el combo de roles
            UsuarioLoguiado usu = (UsuarioLoguiado)HttpContext.Session["user"];

            //pARA EN LA VISTA PODER SABER A QUE  CONTROLADOR EL AJAX  LLAMARA
            ViewBag.ControlCsharp = controler;
            ViewBag.VistaCsharp = vista;


            //si no tiene  el listado formulario devuelvo al loguin
            List<Formulario> _sessformularioParaDesabilitar = new List<Formulario>();
            _sessformularioParaDesabilitar = (List<App_Data.Formulario>)Session["sessformularioParaDesabilitar"];

            if (_sessformularioParaDesabilitar.Count == 0)
            {
                return RedirectToAction("Index", "DashBoard");
            }
            ViewBag.VBformularioParaDesabilitar = _sessformularioParaDesabilitar;

            //validar que si no tiene permiso a este formulario no entre
            List<vw_ListDeFormuriosbyRolyUser> formulariosPlanRoleUser = new List<vw_ListDeFormuriosbyRolyUser>();
            formulariosPlanRoleUser = (List<App_Data.vw_ListDeFormuriosbyRolyUser>)Session["FormulariosPermitidos"];
            //traera false o true;
            var EstaEsteFormulario = formulariosPlanRoleUser.Where(f => f.FormDescripcion == PaginaAutorizada).Any();
            if (formulariosPlanRoleUser.Count == 0)
            {
                return RedirectToAction("Index", "DashBoard");
            }
            //si es diferente de true quiere decir que no tiene permiso
            else if (!EstaEsteFormulario)
            {
                return RedirectToAction("Index", "DashBoard");
            }
            ViewBag.ListaFormulario = formulariosPlanRoleUser;

            ViewBag.listSchedule = datosSchedule();
            //para que el div de accion solo aparezca si son 
            //vistas difrentes a home
            ViewBag.isHome = true;


            






            return View();
        }

        public List<Usar_Cita> datosSchedule()
        {
            List<Usar_Cita> data = new List<Usar_Cita> {
     new Usar_Cita {
            ProgramName = "Turtle Walk",
            Comments = "Night out with turtles",
            ProgramStartTime = new DateTime(2016, 6, 2, 3, 0, 0),
            ProgramEndTime = new DateTime(2016, 6, 2, 4, 0, 0),
            IsAllDay = true
     },
     new Usar_Cita {
            ProgramName = "Winter Sleepers",
            Comments = "Long sleep during winter season",
            ProgramStartTime = new DateTime(2016, 6, 3, 1, 0, 0),
            ProgramEndTime = new DateTime(2016, 6, 3, 2, 0, 0)
     },
     new Usar_Cita {
            ProgramName = "Estivation",
            Comments = "Sleeping in hot season",
            ProgramStartTime = new DateTime(2016, 6, 4, 3, 0, 0),
            ProgramEndTime = new DateTime(2016, 6, 4, 4, 0, 0)
     }
    };
            return data;
        }

    }
}
