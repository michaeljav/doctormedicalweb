﻿using DoctorMedicalWeb.App_Data;
using DoctorMedicalWeb.ModelsComplementarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DoctorMedicalWeb.Libreria;

namespace DoctorMedicalWeb.Controllers
{
    public class DashBoardController : Controller
    {
        DoctorMedicalWebEntities db = new DoctorMedicalWebEntities();
        ResponseModel respuesta = new ResponseModel();
        //
        // GET: /DashBoard/
        //[Authorize]
        public ActionResult Index()
        {

            //para que el div de accion solo aparezca si son 
            //vistas difrentes a home
            ViewBag.isHome = true;
            UsuarioLoguiado u = (UsuarioLoguiado)HttpContext.Session["user"];
            //Si NO esta loguieado lo redireccionara al loguin
            if (HttpContext.Session["user"] == null)
            {
                return RedirectToAction("Index", "PaginaPresentacion");
            }

            //si no tiene  el listado formulario devuelvo al loguin
            List<Formulario> _sessformularioParaDesabilitar = new List<Formulario>();
            _sessformularioParaDesabilitar = (List<App_Data.Formulario>)Session["sessformularioParaDesabilitar"];

            if (_sessformularioParaDesabilitar.Count == 0)
            {
                return RedirectToAction("Index", "DashBoard");
            }
            ViewBag.VBformularioParaDesabilitar = _sessformularioParaDesabilitar;




            //si no tiene este usuario formulario asignados devuelvo al loguin
            List<vw_ListDeFormuriosbyRolyUser> formulariosPlanRoleUser = new List<vw_ListDeFormuriosbyRolyUser>();
           formulariosPlanRoleUser=   (List<App_Data.vw_ListDeFormuriosbyRolyUser>)Session["FormulariosPermitidos"];

           if (formulariosPlanRoleUser==null  || formulariosPlanRoleUser.Count == 0)
           {
               HttpContext.Session["user"] = null;
               return RedirectToAction("Index", "PaginaPresentacion");
           }
        ViewBag.ListaFormulario = formulariosPlanRoleUser;
          
           
            //ViewBag.NombreCompleto = u.usuario.UsuaNombre + " " + u.usuario.UsuaApellido; 
      
            return View();
        }

    }
}
